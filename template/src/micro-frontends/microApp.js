/**
 * 初始化微应用
 * @param {Vue} inst vue根实例 
 * @param {Object} props 微应用接口
 */
export function initMicroApp(inst, props) {
    // 主应用接口挂载到vue全局实例
    const VueCtor = inst.$options._base
    VueCtor.prototype.$MF = props

    // element-ui size
    const { microApp, } = props
    const size = microApp.getElementSize()
    Object.assign(VueCtor.prototype.$ELEMENT, { size })
}