/**
 * 初始化缓存视图，同步缓存视图改变
 * @param {Vue} inst vue根实例 
 * @param {Object} props 微应用接口
 */
export function initTagsView(inst, props) {
    const { emitter, tagsView } = props

    function setCachedViews() {
        const views = tagsView.getAppCachedViews(process.env.VUE_APP_NAME)
        inst.$store.commit('tagsView/SET_CACHED_VIEWS', views)
    }

    function refreshView(view) {
        if (tagsView.isAppView(process.env.VUE_APP_NAME, view)) {
            inst.$store.commit('tagsView/REFRESH_VIEW_KEY')
        }
    }

    emitter.on(tagsView.EVENT_CACHED_CHANGE, setCachedViews)
    emitter.on(tagsView.EVENT_REFRESH_VIEW, refreshView)

    setCachedViews()

    inst.$once('hook:beforeDestroy', () => {
        emitter.off(tagsView.EVENT_CACHED_CHANGE, setCachedViews)
        emitter.on(tagsView.EVENT_REFRESH_VIEW, refreshView)
    })
}