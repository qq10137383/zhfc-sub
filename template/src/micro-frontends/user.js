/**
 * 初始化用户信息，同步用户信息改变
 * @param {Vue} inst vue根实例 
 * @param {Object} props 微应用接口
 */
export function initUser(inst, props) {
    const { emitter, user, auth } = props

    function setUserInfo() {
        const userInfo = user.getUserInfo()
        const token = auth.getToken()
        inst.$store.commit('user/SET_USER_INFO', userInfo)
        inst.$store.commit('user/SET_TOKEN', token)
    }

    emitter.on(user.EVENT_USER_INFO_CHANGE, setUserInfo)

    setUserInfo()

    inst.$once('hook:beforeDestroy', () => {
        emitter.off(user.EVENT_USER_INFO_CHANGE, setUserInfo)
    })
}