import { initMicroApp } from './microApp'
import { initTagsView } from './tagsView'
import { initUser } from './user'

let mainProps

/**
 * 初始化微前端
 * @param {Vue} inst vue根实例 
 * @param {Object} 主应用接口
 */
export function init(inst, props) {
    mainProps = props
    initMicroApp(inst, props)
    initTagsView(inst, props)
    initUser(inst, props)
}

/**
 * 激活微前端
 * @param {Vue} inst vue根实例 
 * @param {Object} 主应用接口
 */
// eslint-disable-next-line no-unused-vars
export function active(inst, props) {
    window.dispatchEvent(new Event('resize'))
}

/**
 * 反激活微前端
 * @param {Vue} inst vue根实例 
 * @param {Object} 主应用接口
 */
// eslint-disable-next-line no-unused-vars
export function deactive(inst, props) {
}

/**
 * 获取主应用接口
 * @returns 
 */
export function getMainProps() {
    return mainProps
}