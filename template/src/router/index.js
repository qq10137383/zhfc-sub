import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/',
    name: "Portal",
    component: () => import("@/views/portal")
  }
]

/**
 * 获取路由基路径
 * @returns 
 */
function getRouteBase() {
  // 嵌入主应用需要加应用前缀
  if (window.__POWERED_BY_QIANKUN__) return `/${process.env.VUE_APP_NAME}`
  // 开发模式或独立端口部署
  if (process.env.NODE_ENV === 'development' || process.env.VUE_APP_DEPLOY_MODE === '1') return '/'
  // 非独立端口子路径部署
  if (process.env.VUE_APP_DEPLOY_MODE === '2') return process.env.OUTPUT_DIR
}

const createRouter = () => new Router({
  mode: 'history',
  base: getRouteBase(),
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  if (router) {
    const newRouter = createRouter()
    router.matcher = newRouter.matcher // reset router
  }
}

export default router
