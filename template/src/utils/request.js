import axios from 'axios'
import { Message } from 'element-ui'
import store from '@/store'
import { getMainProps } from '@/micro-frontends'

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  withCredentials: true, // send cookies when cross-domain requests
  timeout: 3 * 60 * 1000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent
    const headers = config.headers
    if (store.getters.token) {
      const props = getMainProps()
      props && props.auth.setAuthHeaders(headers)
    }
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response.data

    if (!res.success) {
      Message({
        message: res.msg || 'Error',
        type: 'error',
        duration: 5 * 1000
      })
      return Promise.reject(new Error(res.msg || 'Error'))
    } else {
      return res.data || res.msg
    }
  },
  error => {
    console.log('err' + error) // for debug
    if (error.response) {
      const props = getMainProps()
      switch (error.response.status) {
        case 400:
          Message({
            message: error.response.data && error.response.data.msg || 'Error',
            type: 'error',
            duration: 5 * 1000
          })
          break
        case 401:
          // 返回 401 清除token信息并跳转到登录页面
          props && props.user.resetUserInfo(true)
          break
        case 403:
          // 返回 403 提示无权限
          Message({
            message: '您没有权限访问', // error.message
            type: 'error',
            duration: 3 * 1000
          })
          break
        default:
          Message({
            message: '请求超时，请稍后重试', // error.message
            type: 'error',
            duration: 5 * 1000
          })
      }
    }
    return Promise.reject(error)
  }
)

export default service
