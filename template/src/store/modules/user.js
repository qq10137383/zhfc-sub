const state = {
  token: '', // 验证token
  name: '', // 用户名
  avatar: '', // 头像
  introduction: '', // 简介
  roles: [], // 用户角色
  userInfo: {},  // 用户信息
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_USER_INFO: (state, userInfo) => {
    state.name = userInfo.name
    state.avatar = userInfo.avatar
    state.introduction = userInfo.introduction
    state.roles = [userInfo.usertype]
    state.userInfo = userInfo
  },
}

const actions = {}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
