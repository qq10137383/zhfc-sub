import './public-path'
import Vue from 'vue'

import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import V7 from 'dhcc-v7'

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import './icons' // icon

import * as filters from './filters' // global filters
import * as components from './components' // global components
import * as directives from './directives' // global directives

import * as mf from '@/micro-frontends' // micro frontends

import './patch' // patch

Vue.use(Element)
Vue.use(V7)

// register global components
Object.keys(components).forEach(key => {
  Vue.component(key, components[key])
})
// register global utility filters
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})
// register global directives
Object.keys(directives).forEach(key => {
  Vue.directive(key, directives[key])
})

Vue.config.productionTip = false

// vue实例
let instance = null

// 渲染应用
function render(container) {
  let el = `#${process.env.VUE_APP_NAME}`
  if (container) {
    el = container.querySelector(el)
  }
  instance = new Vue({
    el,
    router,
    store,
    render: h => h(App)
  })
  return instance
}

// 销毁应用
function destroy() {
  if (instance) {
    instance.$destroy()
    instance.$el.innerHTML = ''
  }
  instance = null
}

// ===================== 独立运行 ======================
if (!window.__POWERED_BY_QIANKUN__) {
  render()
}

// ==================== 微应用中运行 ====================

// 启动应用
export async function bootstrap() {
}

// 挂载应用
export async function mount(props) {
  const { options, container } = props
  // keepAlive模式下应用不能重复创建
  if (instance && options.keepAlive) {
    mf.active(instance, props)
  }
  else {
    render(container)
    mf.init(instance, props)
  }
}

// 卸载应用
export async function unmount() {
  const props = mf.getMainProps()
  const { options } = props
  // keepAlive模式下应用不销毁
  if (options.keepAlive) {
    mf.deactive(instance, props)
  }
  else {
    destroy()
  }
}